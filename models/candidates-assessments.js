const mongoose = require('mongoose');

const candidatesAssessmentsSchema = new mongoose.Schema({
  assessmentId: String,
  candidateId: String,
  questions:Array
});

const candidatesAssessments = mongoose.model('candidates_assessments', candidatesAssessmentsSchema);

exports = module.exports = candidatesAssessments;
