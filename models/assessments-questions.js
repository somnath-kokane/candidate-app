const mongoose = require('mongoose');

const assessmentsQuestionsSchema = new mongoose.Schema({
  assessmentId: String,
  questionId: String
});

const assessmentsQuestions= mongoose.model('assessments_questions', assessmentsQuestionsSchema);

exports = module.exports = assessmentsQuestions;
