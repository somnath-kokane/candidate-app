const mongoose = require('mongoose');

const questionsSchema = new mongoose.Schema({
  question: String,
});

const questions= mongoose.model('questions', questionsSchema);

exports = module.exports = questions;
