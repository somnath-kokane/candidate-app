
const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const app = express()
const routes = require('./routes')

const MONGODB_URI = 'mongodb://localhost:27017/ramdata'

app.set('port', (process.env.PORT || 3000));

app.use('/', express.static(path.join(__dirname, '../public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

routes.init(app)

mongoose.connect(MONGODB_URI)
const db = mongoose.connection
mongoose.Promise = global.Promise

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log('Connected to MongoDB');

  // setRoutes(app);

  // app.get('/*', function (req, res) {
  //   res.sendFile(path.join(__dirname, '../public/index.html'));
  // });

  app.listen(app.get('port'), () => {
    console.log('Angular Full Stack listening on port ' + app.get('port'));
  });

});
