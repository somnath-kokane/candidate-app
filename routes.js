const jwt = require('jsonwebtoken')
const express = require('express')
const mongoose = require('mongoose');
const {ObjectId} = mongoose.Types

const usersModel = require('./models/users')
const assmtQsModel = require('./models/assessments-questions')
const qModel = require('./models/questions')
const candtAssmtModel = require('./models/candidates-assessments')
const assmtModel = require('./models/assessments')

var privateKey = '37LvDSm4XvjYOh9Y';
var apiRoutes = express.Router()
apiRoutes.use(verifyToken)

apiRoutes
  .route('/candidates/:candidateId/assessments/:assessmentId/questions/')
  .get(getQuestions)
  .post(setAnswers)

apiRoutes.get('/assessments/:assessmentId/questions/', getQuestions)
apiRoutes.get('/candidates/:candidateId/assessments/', getAssessments)

function login(req, res, next){
  let {username, password} = req.body
  usersModel.findOne({username}, (err, user) => {
    if(err){
      next(err)
    } else if(user.password !== password) {
      next(err)
    } else {
      let userId = user._id
      let token = jwt.sign({userId}, privateKey)
      res.json({userId, token})
    }
  })
}

function verifyToken(req, res, next){

  console.log('req.headers\n\n', req.headers)

  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if(!token){
    const err = new Error('invalid Token')
    next(err)
  } else {
    jwt.verify(token, privateKey, (err, decoded) => {
      if(err){
        err.statusCode = 401
        next(err)
      } else {
        req.decoded = decoded
        next()
      }
    })
  }
  
}

function getAssessments(req, res, next){
  let {candidateId} = req.params
  candtAssmtModel.find({candidateId}, (err, docs) => {
    if(err){
      next(err)
    } else {
      aIds = docs.map(doc => {
        return ObjectId(doc.assessmentId)
      })
      assmtModel.find({_id: {$in: aIds}}, (err, docs) => {
        if(err){
          next(err)
        } else {
          res.json(docs)
        }
      })
    }
  })
}

function getQuestions(req, res, next){
 let {assessmentId} = req.params
 assmtQsModel.find({assessmentId}, (err, docs) => {
   if(err){
     next(err)
   } else {
    const qIds = docs.map(doc => {
      return ObjectId(doc.questionId)
    })
    if(qIds.length){
      qModel.find({_id: {$in: qIds}}, (err, docs) => {
        if(err){
          next(err)
        } else {
          res.json(docs)
        }
      })
    } else {
      res.json([])
    }
  }
   
 })
}

function setAnswers(req, res, next){
  let {assessmentId, candidateId} = req.params
  let questions = req.body
  candtAssmtModel.findOneAndUpdate({assessmentId, candidateId}, {questions}, {upsert: true}, (err, doc) => {
    if(err){
      next(err)
    } else {
      console.log('doc\n\n', doc)
      res.json(doc)
    }
  })
}

exports.init = function(app){
  app.post('/login/', login)  
  app.use('/api', apiRoutes) 
  app.use((err, req, res, next) => {
    console.log('erro.......', err.stack)
    res.status(err.statusCode || 500)
    res.json(err)
  })
}

