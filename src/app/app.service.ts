import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

const defaultState = {
  loading: false,
  alertMessages: [],
  user: {
    user: undefined,
    loggedIn: false
  }
}

@Injectable()
export class AppService {
  // state:any
  store:BehaviorSubject<any>
  stateChanges:Observable<any>
  constructor() {

    this.store = new BehaviorSubject(defaultState)
    this.stateChanges = this.store.asObservable()
  }

  get state(): any{
    return this.store.value || defaultState
  }

  isLoading(loading:boolean){
    const state = this.state
    state.loading = loading
    this.setState(state)
  }

  setAlertMessage(success, message){
    const state = this.state
    const alertMessages = state.alertMessages || []
    let msgObj = {success, message}
    alertMessages.push(msgObj)
    state.alertMessages = [...alertMessages]
    this.setState(state)
  }

  removeAlertMessages(msgObj){
    const state = this.state
    const alertMessages = state.alertMessages || []
    let idx = alertMessages.indexOf(msgObj)
    if(idx !== -1){
      alertMessages.splice(idx, 1)
      state.alertMessages = [...alertMessages]
    }
    this.setState(state)
  }

  setUser(userObj:any){
    const state = this.state
    state.user  = userObj
    this.setState(state)
  }

  getUserToken(){
    let token = this.state.user.user.token || ''
    return token
  }

  getUser(){
    return this.state.user.user || {}
  }

  setState(state){
    this.store.next({...state})
  }
}