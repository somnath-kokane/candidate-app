import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http'
import {ReactiveFormsModule} from '@angular/forms'
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login';
import {LoadingComponent} from './components/loading/loading'

import {UserService} from './user.service'
import {AppService} from './app.service'
import {AssmtsService} from './assmts.service'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    ModalModule.forRoot()
  ],
  providers: [AppService, UserService, AssmtsService],
  bootstrap: [AppComponent],
  entryComponents:[LoginComponent, LoadingComponent]
  
})
export class AppModule { }
