import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http'
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {AppService} from './app.service'

@Injectable()
export class AssmtsService {
  store:BehaviorSubject<any>
  stateChanges:Observable<any>
  constructor(
    private http:Http,
    private appService: AppService
  ) { 
    this.store = new BehaviorSubject([])
    this.stateChanges = this.store.asObservable()
  }

  get state(){
    return this.store.value
  }

  getQuestions(assmtId){
    const token = this.appService.getUserToken()
    const headers = new Headers({
      'Content-Type': 'application/json', 'charset': 'UTF-8',
      'X-Access-Token': token
    });
    const options = new RequestOptions({ headers });
    this.http.get(
      `/api/assessments/${assmtId}/questions/`,
      options
    ).subscribe(res => {
      const assmts = this.store.value || []
      const assmt = assmts.find(assmt => assmt._id === assmtId)
      assmt.questions = res.json()
      this.setAssmts(assmts)
    })
  }

  answerQuestions(assmtId, questions){
    const token = this.appService.getUserToken()
    const {userId} = this.appService.getUser()
    
    const headers = new Headers({
      'Content-Type': 'application/json', 'charset': 'UTF-8',
      'X-Access-Token': token
    });
    const options = new RequestOptions({ headers });
    this.http.post(
      `/api/candidates/${userId}/assessments/${assmtId}/questions/`,
      JSON.stringify(questions),
      options
    ).subscribe(res => {
      this.appService.setAlertMessage(true, 'submited successfully')
    })
  }

  getAssmts(candidateId){
    const token = this.appService.getUserToken()
    const headers = new Headers({
      'Content-Type': 'application/json', 'charset': 'UTF-8',
      'X-Access-Token': token
    });
    const options = new RequestOptions({ headers });
    this.http.get(
      `/api/candidates/${candidateId}/assessments/`,
      options
    ).subscribe(res => {
      let assmts = res.json()
      this.setAssmts(assmts)
    })
  }

  setAssmts(assmts){
    this.setState(assmts)
  }

  setState(state){
    this.store.next([...state])
  }

}